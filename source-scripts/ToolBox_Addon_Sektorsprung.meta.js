﻿// ==UserScript==
// @name           ToolBox_Addon_Sektorsprung
// @author         Trinitroglycerol
// @description    Landepunkt für Spieler berechnen
// @namespace      https://prodgame*.alliances.commandandconquer.com/*/index.aspx*
// @include        https://prodgame*.alliances.commandandconquer.com/*/index.aspx*
// @version        0.1.3 Beta
// @grant          none
// @downloadURL    https://del.a-y-c-e.de/scripts/source/ToolBox_Addon_Sektorsprung.user.js
// @updateURL		https://del.a-y-c-e.de/scripts/source/ToolBox_Addon_Sektorsprung.meta.js
// ==/UserScript==