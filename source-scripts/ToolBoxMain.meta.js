﻿// ==UserScript==
// @name           ToolBoxMain
// @author         Trinitroglycerol
// @description    Hauptmenü für die ToolBoxAddons
// @namespace      https://prodgame*.alliances.commandandconquer.com/*/index.aspx*
// @include        https://prodgame*.alliances.commandandconquer.com/*/index.aspx*
// @version        0.1 Beta
// @grant          none
// @downloadURL    https://del.a-y-c-e.de/scripts/source/ToolBoxMain.user.js
// @updateURL		https://del.a-y-c-e.de/scripts/source/ToolBoxMain.meta.js
// ==/UserScript==