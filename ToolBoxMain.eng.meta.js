﻿// ==UserScript==
// @name           ToolBoxMain
// @author         Trinitroglycerol
// @description    Main menu for the ToolBoxAddons
// @namespace      https://cncapp*.alliances.commandandconquer.com/*/index.aspx*
// @include        https://cncapp*.alliances.commandandconquer.com/*/index.aspx*
// @version        0.1.1 Beta (Eng)
// @grant          none
// @downloadURL    https://gitlab.com/rongoree/cncta-user-scripts/raw/master/ToolBoxMain.eng.user.js
// @updateURL		https://gitlab.com/rongoree/cncta-user-scripts/raw/master/ToolBoxMain.eng.meta.js
// ==/UserScript==
