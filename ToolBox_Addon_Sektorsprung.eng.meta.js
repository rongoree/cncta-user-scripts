﻿// ==UserScript==
// @name           ToolBox_Addon_Sektorsprung
// @author         Trinitroglycerol
// @description    Calculates sector jump landing points
// @namespace      https://cncapp*.alliances.commandandconquer.com/*/index.aspx*
// @include        https://cncapp*.alliances.commandandconquer.com/*/index.aspx*
// @version        0.1.4 Beta (Eng)
// @grant          none
// @downloadURL    https://gitlab.com/rongoree/cncta-user-scripts/raw/master/ToolBox_Addon_Secktorsprung.eng.user.js
// @updateURL		https://gitlab.com/rongoree/cncta-user-scripts/raw/master/ToolBox_Addon_Secktorsprung.eng.meta.js
// ==/UserScript==
